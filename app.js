let count;
const $mainList = $(".mainList");     //reference for main list
var $divList = $(".parent");          //reference for parent div
var $inputBox = $("#inputbox");       //reference for input box
var $document = $(document)
var myStorage = window.localStorage; //reference for local storage
 
//function for showing task List if any task stored in localStorage

function showList() {
    let arr = [];

    const storageLen = myStorage.length;
    if (storageLen > 0) { 
        for (let i = 0; i < storageLen; i++) {
            // arr.push(localStorage.getItem(i + 1).split(",")[3]);
            const key = myStorage.key(i);
            const localArr = myStorage.getItem(key).split(",");

            arr.push(localArr[3]);
        }
        arr.sort(function (a, b) {
            return a - b;
        });
    }

    const arrlen = arr.length;
    var temp1=''
    for (var i = arrlen - 1; i >= 0; i--) {
        for (let j = 0; j < storageLen; j++) {
            const key = myStorage.key(j);
            const localArray = myStorage.getItem(key).split(",");
            if (arr[i] === localArray[3]) {
                const divList1 = $divList.clone();
                divList1.attr("data-id", key);
                var listItemData = divList1.find(".listItemData");
                listItemData.find(".data").text(localArray[0]);
                listItemData.find(".creationDate").text(`Date: ${localArray[1]}`);
                listItemData.find(".creationTime").text(`Time: ${localArray[2]}`);
                divList1.css("display", "flex"); 
                temp1+=divList1.prop('outerHTML')
                
            }
        }
    }
    $mainList.append(temp1);
}

showList();



if (myStorage.length) {
    let count1 = [];
    for (let i = 0; i < myStorage.length; i++) {
        count1.push(myStorage.key(i));
    }
    count1 = count1.sort((a, b) => b - a);
    count = count1[0];
} else {
    count = 0;
}

//creating new Task

$document.on("click", ".addBtn", function () {
    count++;
    var inputValue = $inputBox.val();
    var dateTime = getTime();
    const nowdate = Date.now();
    myStorage.setItem(count, [inputValue, dateTime[0], dateTime[2], nowdate]);
    const divList2 = createElement(count)
    $mainList.prepend(divList2);
    $inputBox.val("");
});

// edit button function

$document.on("click", "#editButton", function () {

    var temp = $(this)
        .parents(".parent")
        .find(".listItemData")
        .find(".listItemContent")
        .html();

    $inputBox.val(temp.trim());
    var btn = $(this).parents().find(".addBtn");
    btn.html("Update Task");
    btn.attr("class", "updateTask");
    btn.attr("disabled", false);
    $(this).parents(".parent").find(".listItemData").addClass("update-data");
});

// updating the  task

$document.on("click", ".updateTask", function () {
    const $updateData = $(".update-data").parents(".parent")
    var temp1 = $inputBox.val();
    var dateTime = getTime();
    const nowDate = Date.now();
    const id = $updateData.attr("data-id")
    myStorage.setItem(id, [temp1, dateTime[0], dateTime[2], nowDate]);

    const divList3 = createElement(id)
    $updateData.remove();
    $mainList.prepend(divList3);
    $inputBox.val("");

    $(".updateTask")
        .removeClass("updateTask")
        .addClass("addBtn")
        .html("Add Task");


});

//deleting the task

$document.on("click", "#deleteButton", function () {
    const $deletingElement= $(this).parents(".parent")
    const id = $deletingElement
        .find(".listItemData")
        .parents(".parent")
        .attr("data-id");

    myStorage.removeItem(id);
    $deletingElement.remove();
    //   count--;
});


// function for getting time and date

function getTime() {     
    var currentdate = new Date();
    var date =
        currentdate.getDate() +
        "/" +
        (currentdate.getMonth() + 1) +
        "/" +
        currentdate.getFullYear() +
        "  " +
        currentdate.getHours() +
        ":" +
        currentdate.getMinutes() +
        ":" +
        currentdate.getSeconds();
    const dateTime = date.split(" ");
    return dateTime;
}

// function for creating new node for task
function createElement(num) { 
    const divList2 = $divList.clone();
    divList2.attr("data-id", num);
    const local = myStorage.getItem(num).split(",");
    var createListItemData = divList2.find(".listItemData");

    createListItemData.find(".data").text(local[0]);
    createListItemData.find(".creationDate").text(`Date: ${local[1]}`);
    createListItemData.find(".creationTime").text(`Time: ${local[2]}`);
    divList2.css("display", "flex");

    return divList2
}